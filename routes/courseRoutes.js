const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js")
const auth = require("../auth.js")

// Route for creating a course 
router.post("/create", auth.verify, (req, res) => {

	// Method #1 
	// contains the data needed for creating a course
	const data = {
		course: req.body, // name, description, price
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin) {

	courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController))

	} else {

		res.send(false);
	}

	// Method #2 
	/*
	const userData = auth.decode(req.headers.authorization);


	courseController.addCourse(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController))

	*/

});


// Route for retrieving all courses
router.get("/all", (req, res) =>{

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all active courses 

router.get("/active", (req, res) => {

	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))

});

//Route for retrieving a specific course 
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

// Route for updating a course
router.put("/update/:courseId", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}


	if(data.isAdmin) {

	courseController.updateCourse(data.course, data.params).then(resultFromController => res.send(resultFromController))

	} else {

		res.send(false);
	}
});

// Route for archiving a course activity 
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

	courseController.archiveCourse(data.course, data.params).then(resultFromController => res.send(resultFromController))

	} else {

		res.send(false);
	}

})
module.exports = router;