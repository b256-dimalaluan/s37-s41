const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js")

// Route for checking if the user's email already exists in the database 

router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Router for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});
// Router for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Activity 
router.get("/details", auth.verify, (req, res) => {
/*
	const userData = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
*/
	const userData = auth.decode(req.headers.authorization);


	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Router for enrolling a user 
router.post("/enroll", auth.verify, (req, res) => {

	const data = {
		// userId will be received from the request header 
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enrollUser(data).then(resultFromController => res.send(resultFromController))
});

// mini activity 
// retrieve all users 
router.get("/all", (req, res) =>{

	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
});


module.exports = router;

// retrieve all non admin user 
router.get("/admins", (req, res) => {

	userController.getAdminUsers().then(resultFromController => res.send(resultFromController))

});