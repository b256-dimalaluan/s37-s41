const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, "First Name is required"]
	}, 
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email Address is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled"
			}
		}
	]

	
});

module.exports = mongoose.model("User", userSchema);
/*
.firstName - String
.lastName - String
.email -String
.password - String
.isAdmin - Boolean (Default value - false)
.mobileNo - String
.enrollments - Array of objects
courseId - String
enrolledOn - Date (Default value - new Date object)
status - String (Default value - Enrolled)

Make sure that all the fields are required to ensure consistency in the user information being stored in our database
*/