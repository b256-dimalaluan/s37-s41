// contains all the business logic and functions of our application
const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// Check if the email already exists
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkIfEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0){

			return true; 

		} else {

			return false;

		}
	})
};

// User registration
/*
	Business Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
};

// User authentication
		/*
			Business Logic:
			1. Check the database if the user email exists
			2. Compare the password provided in the login form with the password stored in the database
			3. Generate/return a JSON web token if the user is successfully logged in and return false if not
		*/

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;

		}
		else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
			//example. isSingle, isDone, isAdmin, areDone, etc..

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects

				return {access: auth.createAccessToken(result)}

			} 
			else {

				return false;
			}
		}
	})
};

/*
	data = {
		userId: userData.id
	}
*/

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then((result, err) => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

		result.password = "";


		return result;	
		})

};


// Enrolling a user to a course
/*
	Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enrollUser = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((enrolled, err) => {

			if(err) {

				return false;

			} else {

				return true;
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrolees.push({userId: data.userId});

		return course.save().then((enrolled, err) => {

			if(err) {

				return false;

			} else {

				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {

		return true;
	} else {

		return false;
	}
};

// retrieving all users
module.exports.getAllUsers = () => {

	return User.find({}).then( result => {

		return result;
	})
};

// retrieving all admin users 

module.exports.getAdminUsers = () => {

	return User.find({isAdmin: true}).then( result => {

		return result;
	})
}